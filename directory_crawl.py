# -*- coding: utf-8 -*-
from elasticsearch import Elasticsearch
import os
import multiprocessing
import nltk
from clean_html import clean_html

class CrawlIndexDirectory():

    
    es = Elasticsearch()
    queue = None

    def directory_crawling(self,directory):
        ext = ['.html', '.htm', 'xhtml', 'xml', 'xht']
        for f in os.listdir(directory):
            name = f
            f = directory + "/" + f
            if(f.endswith(tuple(ext))):
                self.queue.put((f, name))
            elif(os.path.isdir(f)):
                self.directory_crawling(f)
        self.queue


    def extract_text_html(self,file_to_index):
        with open(file_to_index, 'r') as f:
            read_data = f.read()
        return clean_html(read_data)


    def indexing_files(self,queue, STOP_TOKEN):
        count = 0
        while True:
            file_to_index, filename = self.queue.get()
            if(file_to_index == STOP_TOKEN):
                print "# of indexed documents : ", count
                print "end of indexing"
                break
            else:
                print count
                count += 1
                text = self.extract_text_html(file_to_index)
                doc = {
                       'name': filename,
                       'text': text
                       }
                #Index in ElasticSearch
                res = self.es.index(index="wikitrauma_index",
                               doc_type='html', id=count, body=doc)

    