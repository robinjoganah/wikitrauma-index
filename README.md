# README #
* J'utilise les librairies : Elasticsearch, BeautifulSoup et NLTK
* mettre le chemin du fichier dans la commande python, ou bien l'éditer dans le main
* OU dans main.py changer l'adresse du dossier pour chercher les fichiers téléchargés à partir de wikitrauma-scraper
* python main.py Directory_Path pour indexer les fichiers
* le code test_elasticsearch.py permet de tester que les fichiers ont bien été indexés dans l'index wikitrauma_index

* Pour l'extraction des liens la commande : python extract_doc_links.py directory_path
* Le directory path dans ce cas est la racine des fichiers téléchargés par le spider '/Users/votrenom/wikitrauma-webscraper/page' par exemple