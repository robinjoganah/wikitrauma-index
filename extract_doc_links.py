# -*- coding: utf-8 -*-
import os
import multiprocessing
from bs4 import BeautifulSoup
import sys

class ExtractLinksFromPages():

    def directory_crawling(self,directory, queue):
        ext = ['.html', '.htm', 'xhtml', 'xml', 'xht']
        for f in os.listdir(directory):
            name = f
            f = directory + "/" + f
            if(f.endswith(tuple(ext))):
                queue.put((f, name))
            elif(os.path.isdir(f)):
                self.directory_crawling(f, queue)


    def extract_links_docs(self,queue, STOP_TOKEN):
        ext = ['.pdf','.doc','.docx']
        doc_links = []
        google_doc_links = []
        iFrames = []
        while True:
            file_to_index, filename = queue.get()
            if(file_to_index == STOP_TOKEN):
                print "nb docs", len(doc_links)
                print "doc links",doc_links[0:5]
                print "nb google docs", len(set(google_doc_links))
                f = open("google_docs.txt","w")
                print "google docs",google_doc_links[0:5]
                for google_doc in list(set(google_doc_links)):
                    f.write(google_doc + "\n")
                print "iframes", iFrames[0:5]
                break
            links = []
            
            html = open(file_to_index)
            soup = BeautifulSoup(html)
            for link in soup.find_all('a'):
                    if link.get('href'):
                        links.append(link.get('href'))
            for iframe in soup("iframe"):
                links.append(soup.iframe.get('src'))    
            for link in links:
                if(link.endswith(tuple(ext))):
                    doc_links.append(link)
                elif(link.startswith("https://docs.google.com/document/")
                    or link.startswith("https://drive.google.com/")):
                    google_doc_links.append(link)
        

if __name__ == '__main__':
    directory_path = '/Users/robinjoganah/wikitrauma-webscraper/page'
    STOP_TOKEN = "STOP"
    extractor = ExtractLinksFromPages()
    queue = multiprocessing.Queue()
    pool = multiprocessing.Pool(processes=1)
    extracting_process = multiprocessing.Process(target=extractor.extract_links_docs,
                                               args=(queue, STOP_TOKEN))
    extracting_process.start()
    if(sys.argv > 1):
        extractor.directory_crawling(sys.argv[1],queue)
    else:
        extractor.directory_crawling(directory_path,
        queue)
    queue.put((STOP_TOKEN, None))
