import multiprocessing
from directory_crawl import CrawlIndexDirectory
import sys

def main():
    crawling_path = "/Users/robinjoganah/wikitrauma-webscraper/pages"
    if(len(sys.argv)>1):
        crawling_path = sys.argv[1]
    STOP_TOKEN = "STOP"
    crawler_indexer = CrawlIndexDirectory()
    queue = multiprocessing.Queue()
    pool = multiprocessing.Pool(processes=1)
    crawler_indexer.queue = queue
    indexing_process = multiprocessing.Process(target=crawler_indexer.indexing_files,
                                                   args=(queue,STOP_TOKEN))
    indexing_process.start()
    crawler_indexer.directory_crawling(crawling_path)
    crawler_indexer.queue.put((STOP_TOKEN, None))


if __name__ == '__main__':
    main()