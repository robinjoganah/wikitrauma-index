# -*- coding: utf-8 -*-
from elasticsearch import Elasticsearch

es = Elasticsearch()

# res = es.search(index="wikitrauma_index", body={"query": {"match_all": {}}})
res = es.search(index="wikitrauma_index", body={
  "query": {
    "query_string": {
      "query": "tranexam"
}}})


print("Got %d Hits:" % res['hits']['total'])
print res
for hit in res['hits']['hits']:
    print("%(name)s : %(text)s" % hit["_source"]).encode('utf-8')
    break

"""
curl -XGET 'http://localhost:9200/wikitrauma_index/attachment/_search?vector' -d ' 
{ 
"fields": [ 
"file" 
] 
} 
"""